import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'xyz.canardoux.danku_vue_io',
  appName: 'danku',
  webDir: 'dist',
  server: {
    androidScheme: 'https'
  }
};

export default config;
